--City Skate--

C'est un mini jeu réalisé en Javascript. 
Projet très intéressant qui m'a permis d'assimiler quelques notions sur la Programmation Orientée Objet.
Il y a une classe Hero qui permet d'utiliser différentes animations pour le personnage principal.
Plusieurs fonctions comme des setTimeout pour éviter que les animations se jouent en boucle.

Ce projet a encore des voies d'améliorations, notamment sur les collisions.
J'aurai aimé pouvoir intégrer un tableau des scores qui enregistre le top 10 des meilleurs scores avec identifiant, mais à ce stade de ma formation je ne connais pas vraiment la programmation de base de données.
Egalement des fonctions math.random qui me permettent de faire en sorte que les obstacles apparaissent aléatoirement.

Néanmoins, ce projet m'a inspiré d'autres jeux que je réaliserai prochainement.

