
// création de l'objet Hero qui va permettre d'afficher differentes animations
class Hero {
  _roulerElement;
  _sauterElement;
  _currentAnimation;
  _rect;
  _score = 0;
  _action;
  
  constructor() {
    this._roulerElement = document.getElementById('rouler');
    this._sauterElement = document.getElementById('sauter');
    this._tomberElement = document.getElementById('gameover');
    this._currentAnimation = 'rouler';
    this._rect = this._roulerElement.getBoundingClientRect();
    
  }

  get rect() {
    if( this._currentAnimation == "rouler" ) {
      // getBoundingClientRect permet de récupérer les dimensions et la position de l'élément afin de gérer des collisions
      return this._roulerElement.getBoundingClientRect();
    }
    if( this._currentAnimation == "sauter" ) {
      this._sauterElement.getBoundingClientRect();
      return this._sauterElement.getBoundingClientRect();
    }
      
      return this._rect;
  }

  get action(){
    return this._action;
  }
  get score(){
    return this._score;
  }
  // les differentes conditions pour gérer les actions/animations du personnage
  set action(action) {
    this._action = action;
    if (action == "rouler") {
      this._roulerElement.style.display = 'block';
      this._sauterElement.style.display = 'none';
      this._currentAnimation = 'rouler';
    } else if (action == "sauter") {
      this._roulerElement.style.display = 'none';
      this._sauterElement.style.display = 'block';
      this._currentAnimation = 'sauter';
      const audioElement = document.getElementById('sauterson');
      // ajout de son
      audioElement.currentTime = 0;
      audioElement.volume = 0.5;
      audioElement.play();
      // gérer le score
      this._score = this._score + 100;
      console.log(this._score);
      let afscore = document.getElementById('score');
      afscore.style.display = 'block';
      // setTimeout permet qu'au bout d'un certains temps, ici 800 milli-secondes, des actions se produisent, ici certains éléments sont cachés et d'autres affichés 
      setTimeout(function () {
        let rouler = document.getElementById('rouler');
        let sauter = document.getElementById('sauter');
        sauter.style.display = 'none';
        rouler.style.display = 'block';
        let afscore = document.getElementById('score');
        afscore.style.display = 'none';
      }, 800)
      this._currentAnimation = 'rouler';
    } else if (action == "gameover") {
      this._roulerElement.style.display = 'none';
      this._sauterElement.style.display = 'none';
      this._tomberElement.style.display = 'block';
      this._currentAnimation = 'gameover';
      const tomberElement = document.getElementById('tomberson');
      tomberElement.currentTime = 0;
      tomberElement.volume = 0.5;
      tomberElement.play();
      // ici setTimeout redirige entre guillemet vers un autre écran mais c'est tout simplement des display
      setTimeout(function () {
        let imgscore = document.getElementById('imgscore');
        let scorefinal = document.getElementById('scorefinal');
        let play = document.getElementById('play');
        imgscore.style.display= 'block';
        scorefinal.style.display = 'block';
        // résultat final
        scorefinal.innerHTML = "Tu as fais "+ hero._score +" points";
        play.style.display = 'block';
      }, 1500)
    }
  }
}

// création des different ennemis en l'occurence un plot, une poubelle et un cochon
class Plot {
  _plot;
  _rect;

  constructor() {
    this._plot = document.getElementById('plot');
    this._rect = this._plot.getBoundingClientRect();
  }

  get rect() {
    // il faut bien return cet element pour que la position du plot se mette à jour
    return this._plot.getBoundingClientRect();
  }


  set collision(collision) {
    if (hero.action == "gameover") {
      collision(this._roulerElement, this._plot);
    }
  }
}

class Poubelle {
  _poubelle;
  _rect;

  constructor() {
    this._poubelle = document.getElementById('trash');
    this._rect = this._poubelle.getBoundingClientRect();
  }
  get rect() {
    return this._poubelle.getBoundingClientRect();
  }
  set collision(collision) {
    if (hero.action == "gameover") {
      collision(this._roulerElement, this._poubelle);
    }
  }
}

class Cochon {
  _cochon;
  _rect;

  constructor() {
    this._cochon = document.getElementById('cochon');
    this._rect = this._cochon.getBoundingClientRect();
  }
  get rect() {
    return this._cochon.getBoundingClientRect();
  }
  set collision(collision) {
    if (hero.action == "gameover") {
      collision(this._roulerElement, this._cochon);
    }
  }
}

// création des instances pour afficher les éléments à l'écran
const hero = new Hero();
const plot = new Plot();
const poubelle = new Poubelle();
const cochon = new Cochon();


function initGame(){
  // cette fonction permet de gérer les actions en fonction de la touche appuyée
  document.addEventListener('keypress', function (event) {
    
    if (event.keyCode === 32 && hero._currentAnimation === 'rouler') {
      hero.action = 'sauter';
    } 
  });
}
function updateGame() { //cette fonction permet d'améliorer la fluidité du jeu avec un requestAnimationFrame()
  
  if(hero.action !== "gameover"){
    requestAnimationFrame(updateGame);
    detecterCollision(hero, plot);
  }
}

// gérer les collisions
function detecterCollision(hero, plot) {  
  if (hero.rect.x < plot.rect.x &&
    hero.rect.x + hero.rect.width > plot.rect.x &&
    hero.rect.y < plot.rect.y + plot.rect.height &&
    hero.rect.y + hero.rect.height > plot.rect.y
  ) {
    hero._tomberElement.style.display = 'block';
    hero.action = "gameover";
  } else if (hero.rect.x < poubelle.rect.x &&
    hero.rect.x + hero.rect.width > poubelle.rect.x &&
    hero.rect.y < poubelle.rect.y + poubelle.rect.height &&
    hero.rect.y + hero.rect.height > poubelle.rect.y) {
    hero._tomberElement.style.display = 'block';
    hero.action = "gameover";
  } else if (hero.rect.x < cochon.rect.x &&
    hero.rect.x + hero.rect.width > cochon.rect.x &&
    hero.rect.y < cochon.rect.y + cochon.rect.height &&
    hero.rect.y + hero.rect.height > cochon.rect.y) {
    hero._tomberElement.style.display = 'block';
    hero.action = "gameover";
  }
}

initGame();
updateGame();

hero.score;
